<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>PHP Activity 1</title>
</head>
<body>
  <h1>Full Address</h1>
  <p>
    <?php echo getFullAddress(
      "Philippines",
      "Quezon City",
      "Metro Manila",
      "Katipunan Avenue"
    ); ?>
  </p>
  <p>
    <?php echo getFullAddress(
      "Philippines",
      "Manila",
      "Metro Manila",
      "2401 Taft Ave"
    ); ?>
  </p>

  <h1>Letter-Based Grading</h1>
  <p>
    <?php echo getLetterGrade(87); ?>
  </p>
  <p>
    <?php echo getLetterGrade(94); ?>
  </p>
  <p>
    <?php echo getLetterGrade(74); ?>
  </p>

</body>
</html>
